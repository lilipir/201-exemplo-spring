package br.com.itau.filmes.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import br.com.itau.filmes.models.Filme;

@Service
public class FilmeService {
	private ArrayList<Filme> filmes;

	public FilmeService() {
		filmes = new ArrayList<>();

		Filme filme1 = new Filme();
		filme1.setId(1);
		filme1.setTitulo("Os Trapalhões no Planalto dos Macacos");
		filme1.setAno(1976);

		Filme filme2 = new Filme();
		filme2.setId(2);
		filme2.setTitulo("Xuxa: Lua de Cristal");
		filme2.setAno(1990);
		
		filmes.add(filme1);
		filmes.add(filme2);
	}

	public ArrayList<Filme> obterFilmes() {
		return filmes;
	}
	
	public Filme obterFilme(int id) {
		for(Filme filme: filmes) {
			if(filme.getId() == id) {
				return filme;
			}
		}
		
		return null;
	}
	
	public void inserirFilme(Filme filme) {
		filmes.add(filme);
	}
}
